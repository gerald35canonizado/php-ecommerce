function validate_reg_form(){
	errors = 0;
	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let email = document.querySelector("#email").value;
	let address = document.querySelector("#address").value;
	let username = document.querySelector("#username").value;
	let password = document.querySelector("#password").value;
	let confirm = document.querySelector("#confirm").value;
	// NO EMPTY FIELDS, USERNAME MUST BE MINIMUM 8 CHARACTERS AND MAX 24
	// PASSWORD MUST BE AT LEAST 8 CHARACTERS LONG
	// CONFIRM MUST BE EQUAL TO PASSWORD
	if (firstName == "") {
		document.querySelector("#firstName").nextElementSibling.innerHTML="Please input a valid First Name"
		errors++;
	}else{
		document.querySelector("#firstName").nextElementSibling.innerHTML=""
	}

	if (lastName == "") {
		document.querySelector("#lastName").nextElementSibling.innerHTML="Please input a valid Last Name"
		errors++;
	}else{
		document.querySelector("#lastName").nextElementSibling.innerHTML=""
	}

	if (email == "") {
		document.querySelector("#email").nextElementSibling.innerHTML="Please input a valid Email"
		errors++;
	}else{
		document.querySelector("#email").nextElementSibling.innerHTML=""
	}

	if (address == "") {
		document.querySelector("#address").nextElementSibling.innerHTML="Please input a valid Address"
		errors++;
	}else{
		document.querySelector("#address").nextElementSibling.innerHTML=""
	}

	if (username == "") {
		document.querySelector("#username").nextElementSibling.innerHTML="Please input a valid Username"
		errors++;
	}else{
		document.querySelector("#username").nextElementSibling.innerHTML=""
	}

	if (password == "") {
		document.querySelector("#password").nextElementSibling.innerHTML="Please input a valid Password"
		errors++;
	}else{
		document.querySelector("#password").nextElementSibling.innerHTML=""
	}

	if (confirm == "") {
		document.querySelector("#confirm").nextElementSibling.innerHTML="Please confirm your password"
		errors++;
	}else{
		document.querySelector("#confirm").nextElementSibling.innerHTML=""
	}

	if (username.length < 8 || username.length > 24) {
		document.querySelector("#username").nextElementSibling.innerHTML="Please input a valid username"
		errors++;
	}else{
		document.querySelector("#username").nextElementSibling.innerHTML=""
	}

	if (password.length < 8) {
		document.querySelector("#password").nextElementSibling.innerHTML="Please input a valid password"
		errors++;
	}else{
		document.querySelector("#password").nextElementSibling.innerHTML=""
	}

	if (password !== confirm) {
		document.querySelector("#confirm").nextElementSibling.innerHTML="Please input a valid password"
		errors++;
	}else{
		document.querySelector("#confirm").nextElementSibling.innerHTML=""
	}

	if (errors > 0) {
		return false
	} else {
		return true
	}
}

document.querySelector('#registerUser').addEventListener("click", ()=> {
		if(validate_reg_form()){
		let firstName = document.querySelector("#firstName").value;
		let lastName = document.querySelector("#lastName").value;
		let email = document.querySelector("#email").value;
		let address = document.querySelector("#address").value;
		let username = document.querySelector("#username").value;
		let password = document.querySelector("#password").value;
	
		let data = new FormData;
	
		data.append("firstName", firstName);
		data.append("lastName", lastName);
		data.append("email", email);
		data.append("address", address);
		data.append("username", username);
		data.append("password", password);
	
		fetch("../../controllers/process_register_user.php", {
			method: "POST",
			body: data
		}).then(response=>{
			return response.text();
		}).then(data_from_fetch=>{
			console.log(data_from_fetch);
			if (data_from_fetch == "user_exists"){
				document.querySelector("#email").nextElementSibling.innerHTML = "User already exists";
			}else{
				window.location.replace("../../views/login.php");
			}
		})
	}
})