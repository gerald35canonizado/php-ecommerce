// capture all inputs
// attach eventlistener each input
// if we remove focus from input field it should submit the form

let quantityInputs = document.querySelectorAll(".quantityInput");
quantityInputs.forEach(editInput=>{
	editInput.addEventListener("change",() =>{
		editInput.parentElement.submit();
	})
})