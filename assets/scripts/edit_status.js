let currentStatus = document.querySelectorAll('.currentStatus');
currentStatus.forEach(indiv_status =>{
	indiv_status.addEventListener("click", indiv_status =>{
		indiv_status.target.innerHTML = "";
		indiv_status.target.nextElementSibling.style.display="block";
	})
})

let allSelects  = document.querySelectorAll(".selectStatus");
allSelects.forEach(indiv_select=>{
	indiv_select.addEventListener("blur", indiv_select =>{
		let transaction_code = indiv_select.target.parentElement.parentElement.firstElementChild.innerHTML;
		let status_id = indiv_select.target.value;
		let data = new FormData;
		data.append("transaction_code", transaction_code);
		data.append("status_id", status_id);
		fetch("../../controllers/process_update_status.php", {
			method: "POST",
			body: data
		}).then(response =>{
			return response.text();
		}).then(data_from_fetch => {
			window.location.replace("../../views/admin_orders.php");
		})
	})
})