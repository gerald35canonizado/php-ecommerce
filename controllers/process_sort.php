<?php 
	session_start();
	// will save the value of $sort in a SESSION VARIABLE so we can use it in our view.
	if (isset($_GET['sort'])) {
		if ($_GET['sort'] == "asc") {
			$_SESSION['sort'] = " ORDER BY price ASC";
		} else {
			$_SESSION['sort'] = " ORDER BY price DESC";
		}
	}
	header("LOCATION: " . $_SERVER['HTTP_REFERER']);
 ?>