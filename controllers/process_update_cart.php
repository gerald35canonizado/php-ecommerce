<?php
// capture item id
// capture quantity
// if first time save the item_id and quantity in a session variable, if not just increase the quantity
	session_start();
	$item_id = $_POST['id'];
	$item_quantity = $_POST['quantity'];

	function getCartSum(){
		return array_sum($_SESSION['cart']);
	}

	// edit quantity
	// check if the id/quantity is from the cart page, if yes set $_SESSION['cart']['$item_id'] =  to the quantity received
	// if not (else) use the code below
	if (isset($_POST['fromCartPage'])) {
		$_SESSION['cart'][$item_id] = $item_quantity;
		header("Location: " . $_SERVER['HTTP_REFERER']);
	} else {
		if (isset($_SESSION['cart'][$item_id])) {
			$_SESSION['cart'][$item_id] += $item_quantity;
		} else {
			$_SESSION['cart'][$item_id] = $item_quantity;
		};
			echo getCartSum();
	}	
	// echo print_r($_SESSION['cart']);
?>