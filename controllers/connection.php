<?php 
	$db_host = "localhost";
	$db_username = "root";
	$db_password = "";
	$db_name = "b46Ecommerce";

	// create connection
	$conn = mysqli_connect($db_host, $db_username, $db_password, $db_name);

	// check the connection
	if (!$conn) {
		die("Connection Failed : " . mysqli_error($conn));
	}
?>