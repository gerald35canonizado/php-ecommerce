<?php 
	require "../controllers/connection.php";
	function validate_form(){
		// validation logic
		// we'll check if each of the field in the form has a value. if no it will increase the $error total and if the $errors total>0, it will return false.
		// check if the file extension of the image is within the acceptable file extensions. if no, it will increase the $errors total > 0, it will return to false.
		$errors = 0;
		$file_types = ["jpg", "jpeg", "png", "gif", "bmp", "svg"];
		$file_ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
		if($_POST['name'] == "" || !isset($_POST['name'])){
			$errors++;
		};
		if ($_POST['price'] <= 0 || !isset($_POST['price'])) {
			$errors++;
		};
		if ($_POST['description'] == "" || !isset($_POST['description'])) {
			$errors++;
		};
		if ($_POST['category_id'] == "" || !isset($_POST['category_id'])) {
			$errors++;
		};
		if ($_FILES['image']['name'] != "") {
			if(!in_array($file_ext, $file_types)){
				$errors++;
			};
		};

		// check if the file extension is in array $file_types

		if ($errors > 0) {
			return false;
		}else{
			return true;
		};
	};
	// validate_form == true
	if(validate_form()){
		// SAVING THE ITEMS TO THE DATABASE
		// * CAPTURE ALL THE DATA FROM $_POST OR $_FILES FOR IMAGE
		// * MOVE UPLOADED IMAGE FILE TO THE ASSETS/IMAGES DIRECTORY
		// * CREATE THE QUERY
		// * USE mysqli_query FUNCTION
		// * GO BACK TO THE CATALOG IF SUCCESFUL
		// * IF UNSUCCESSFUL GO BACK TO add_item_form
		$id = $_POST['id'];
		$name = $_POST['name'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$category_id = $_POST['category_id'];
		$item_query = "SELECT image FROM items WHERE id = $id";
		$image_result = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
		// check if the user uploaded a new image, if not save the old value to $image if yes do the process of moving files
		$image = "";
		if ($_FILES['image']['name']==""){
			$image = $image_result['image'];
		} else {
			$destination = "../assets/images/";
			$file_name = $_FILES['image']['name'];
			move_uploaded_file($_FILES ['image']['tmp_name'], $destination . $file_name);
			$image = $destination . $file_name;			
		};

		$update_item_query = "UPDATE items SET name = '$name', price = $price, description = '$description', image = '$image', category_id = $category_id WHERE id = $id";
		$result = mysqli_query($conn, $update_item_query);
		header("Location: ../views/catalog.php");
		} else {
			header("Location: ". $_SERVER['HTTP_REFERER']);
		}
 ?>