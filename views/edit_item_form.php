<?php 
	require "../templates/template.php";
	function get_content(){
	// need always for database connection
	require "../controllers/connection.php";
	?>
		<h1 class="text-center py-5">EDIT ITEM FORM</h1>
		<div class="container">
			<div class="col-lg-6 offset-lg-3">
				<!-- enctype="multipart/form-data" use when there is image to upload -->
				<form action="../controllers/process_edit_item.php" method="POST" enctype="multipart/form-data">

					<?php
						$item_id = $_GET['id'];
						$item_query = "SELECT * FROM items WHERE id = $item_id";
						$item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
					?>

					<div class="form-group">
						<label for="name">Item Name</label>
						<input type="text" name="name" class="form-control" value="<?php echo $item['name']?>">
					</div>
					<div class="form-group">
						<label for="price">Item Price</label>
						<input type="number" name="price" class="form-control" value="<?php echo $item['price']?>">
					</div>
					<div class="form-group">
						<label for="description">Item Description</label>
						<textarea name="description" class="form-control"><?php echo $item['description']; ?></textarea>
					</div>
					<div class="form-group">
						<label for="image">Item Image</label>
						<img src="<?php echo $item['image']?>" height ="50px" width="50px">
						<input type="file" name="image" class="form-control">
					</div>
					<div class="form-group">
						<label for="category_id">Item Category</label>
						<select name="category_id" class="form-control">
							<!-- get all categories from the field so it is user friendly -->
							<?php
								$category_query = "SELECT * FROM categories";
								$categories = mysqli_query($conn, $category_query);
								foreach ($categories as $indiv_category) {
							?>
							<!-- <?php echo $indiv_category['id']== $item['category_id'] ?"selected" : ""?> -->
							<!-- comparing the category id to the indiv_category id in the foreach statement -->
								<option 
								value=" <?php echo $indiv_category['id'] ?>"<?php echo $indiv_category['id']== $item['category_id'] ?"selected" : ""?>> <?php echo $indiv_category['name']; ?></option>
							<?php
								}
							?>
						</select>
					</div>
					<input type="hidden" value="<?php echo $item['id']?>" name="id">
					<button type="submit" class="btn btn-primary">Edit Item</button>
				</form>
			</div>
		</div>
	<?php
	}
?>