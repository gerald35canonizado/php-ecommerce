<?php 
	require "../templates/template.php";
	function get_content(){
		require "../controllers/connection.php";
?>
	<h1 class="text-center py-5">Cart</h1>
	<hr>
	<div class="table-responsive">
		<table class="table table-striped table-bordered">
			<thead>
				<tr class="text-center">
					<th>Item</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				// to check if we have $_SESSION['cart']
				// get the session cart and display each in a <tr>
				// 		* check data type of session cart (ans. associative array)
				//		* do the loop($data as $key => value)
				// 		* goal is to get the id and the quantity
				// 		* get the item detail thru item_query
				// 		* SELECT * FROM items WHERE id = the id we got from the foreach.
				// 		* use mysqli_query to get individual item
				// 		* get the subtotal by multiplying indiv_item['price'] and quantity we got from the foreach
				// 		* get the total(in order for us to do so, we must initialize $total at the very top)
				// display the details in <tr><td>
				$total=0;
				if (isset($_SESSION['cart'])){
                    foreach($_SESSION['cart']as $item_id => $item_quantity){
                    $item_query = "SELECT * FROM items WHERE id = $item_id";
                    $indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
                    $subtotal = $indiv_item['price'] * $item_quantity;
                    $total += $subtotal;
                ?>
				<tr>
					<td><?php echo $indiv_item['name']?></td>
					<td><?php echo $indiv_item['price']?></td>
					<td>
						<form action="../controllers/process_update_cart.php" method="POST">
							<!-- we need to send the id so we know which item in cart to edit -->
							<input type="hidden" name="fromCartPage" value="true">
							<input type="hidden" name="id" value="<?php echo $item_id?>">
							<input type="number" name="quantity" value="<?php echo $item_quantity?>" class="form-control quantityInput">
						</form>
					</td>
					<td><?php echo $subtotal?></td>
					<td><a href="../controllers/process_remove_item.php?id=<?php echo $indiv_item['id']?>"class="btn btn-danger">Remove From Cart</a></td>
				</tr>
				<?php
					}
				}
				?>
				<tr>
					<td></td>
					<td></td>
					<td><a class="btn btn-danger" href="../controllers/process_empty_cart.php">Empty Cart</a></td>
					<td>Total :<?php echo $total ?> </td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><form action="../controllers/process_checkout.php" method="POST">
						<button class="btn btn-success" type="submit">Pay Via Cod</button>
					</form></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		
	</div>
	<script type="text/javascript" src="../assets/scripts/update_cart.js"></script>
<?php
	}
 ?>