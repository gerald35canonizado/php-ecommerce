<?php 
	require "../templates/template.php";
	function get_content(){
?>
	<h1 class="text-center py-5">Register</h1>
	<div class="col-lg-6 offset-lg-3">
		<form action="" method="POST">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<input type="text" name="firstName" class="form-control" id="firstName">
				<span class="validation"></span>
			</div>
			<div  class="form-group">
				<label for="lastName">Last Name</label>
				<input type="text" name="lastName" class="form-control" id="lastName">
				<span class="validation"></span>				
			</div>
			<div  class="form-group">
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control" id="email">
				<span class="validation"></span>				
			</div>
			<div  class="form-group">
				<label for="address">Address</label>
				<input type="text" name="address" class="form-control" id="address">
				<span class="validation"></span>				
			</div>
			<div  class="form-group">
				<label for="username">Username</label>
				<input type="text" name="username" class="form-control" id="username">
				<span class="validation"></span>
				
			</div>
			<div  class="form-group">
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control" id="password">
				<span class="validation"></span>				
			</div>
			<!-- confirm password -->
				<label for="confirm">Confirm Password</label>
				<input type="password" name="confirm" class="form-control" id="confirm">
				<span class="validation"></span>
		</form>
		<div>
		<button type="button" class="btn btn-warning" id="registerUser">Register</button>
		<p>Already Registered? <a href="login.php">Login</a></p>			
		</div>
	</div>
	<script type="text/javascript" src="../assets/scripts/register.js"></script>
<?php
	}
?>