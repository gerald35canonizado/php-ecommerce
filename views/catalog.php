<?php
	require "../templates/template.php";
	function get_content(){
	// need always for database connection
	require "../controllers/connection.php";
	?>
		<div class="container">
			<div class="row">
				<div class="col-lg-2 py-5">
				<!-- this is for the side bar -->
					<h4>Categories</h4>
					<ul class="list-group">
						<li class="list-group-item"><a href="catalog.php">ALL</a>
						</li>
							<?php
								// call categories
								$categories_query = "SELECT * FROM categories";
								// for filtering
								$categories_list = mysqli_query($conn, $categories_query);
								foreach($categories_list as $indiv_category) {
								?>
									<li class="list-group-item"><a href="catalog.php?category_id=<?php echo $indiv_category['id']?>"><?php echo $indiv_category['name'];?></a></li>
								<?php
								}
							?>
					</ul>
					<!-- sorting -->
					<h4>Sort By</h4>
					<ul class="list-group">
						<li class="list-group-item">
							<a href="../controllers/process_sort.php?sort=asc">Lowest to Highest</a>
						</li>
						<li class="list-group-item">
							<a href="../controllers/process_sort.php?sort=desc">Highest to Lowest</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-10">
					<h1 class="text-center py-5">Catalog Page</h1>
					<div class="row">
						<!-- this for the item list -->
						<?php 
						// steps for retrieving items
						// * create query
						// * use mysqli query to get the result
						// * if array use foreach
						// * if object use mysqli_fetch_assoc to convert the data to associative array
						// 		*.1 use foreach($result as $key=>$value)
							$items_query = "SELECT * FROM items";
							// mysqli_query ($connection variable from connection.php, $query)
							if (isset($_GET['category_id'])) {
								$catId = $_GET['category_id'];
								$items_query .= " WHERE category_id = $catId";
							}
							// SORT
							if (isset($_SESSION['sort'])) {
								$items_query .= $_SESSION['sort'];
							}

							$items = mysqli_query ($conn, $items_query);
							foreach ($items as $indiv_item) {
							?>
								<div class="col-lg-4 py-2">
									<div class="card h-100">
										<img class="card-image-top" src="<?php echo $indiv_item['image'] ?>" alt="image">
										<div class="card-body">
											<h4 class="card-title"><?php echo $indiv_item['name']; ?></h4>
											<p class="card-text"><?php echo $indiv_item['price']; ?>.00</p>
											<p class="card-text"><?php echo $indiv_item['description'] ?></p>
											<?php 
											// process of displaying category
											// * get the category name where id is equal to $indiv_item['category_id']
											// * display the data
												$catId = $indiv_item['category_id'];
												$category_query = "SELECT * FROM categories WHERE id = $catId";
												$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
												?>
												<p class="card-text">Category : <?php echo $category['name']; ?></p>
										</div>
										<?php 
											if (isset($_SESSION['user']) && $_SESSION['user']['role_id']==1) {
											?>
												<div class="card-footer">
													<a href="edit_item_form.php?id=<?php echo $indiv_item['id']?>" class="btn btn-primary">Edit Item</a>
													<a href="../controllers/process_delete_item.php?id=<?php echo $indiv_item['id']?>" class="btn btn-danger">Delete Item</a>
												</div>
											<?php
											} else {
											?>
												<div class="card-footer">
														<!-- ADD CART using php -->
													<!-- <form action="../controllers/process_update_cart.php" method="POST">
														<input type="number" name="quantity" value="1" class="form-control">
														<input type="hidden" name="id" value="<?php echo $indiv_item['id']?>">
														<button type="submit" class="btn btn-primary">Add to Cart</button>
													</form> -->
													<input type="number" class="form-control" value"1">
													<button type="button" class="btn btn-success addToCartBtn" data-id="<?php echo $indiv_item['id']?>">Add to Cart</button>
												</div>											
											<?php
											}
										?>
									</div>
								</div>
							<?php
							}
						 ?>
					</div>		
				</div>
			</div>			
		</div>
		<script type="text/javascript" src="../assets/scripts/add_to_cart.js"></script>
	<?php
	}
?>