<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/flatly/bootstrap.css">
	<title>Audio Video</title>
</head>
<body>
	<?php require "navbar.php"; ?>
	<?php get_content(); ?>
	<?php require "footer.php"; ?>	
</body>
</html>